﻿using System.IO;
using DotNetTestsFeatures.Sync.Typescript;
using DotNetTestsFeatures.Tests.Sources.Sync.Typescript;
using NUnit.Framework;

namespace DotNetTestsFeatures.Tests.Sync.Typescript
{
    public class CsTsAssertTests
    {
        [TestCase("right-test.enum.ts", true)]
        [TestCase("deficient-test.enum.ts", false)]
        [TestCase("exceeded-test.enum.ts", false)]
        [TestCase("right-test-with-comments.enum.ts", true)]
        public void CheckEnum_Test(string tsFileName, bool areEquivalent)
        {
            var directory = Directory.GetParent(Directory.GetCurrentDirectory());

            while (directory.Name != "DotNetFeatures.Test")
            {
                directory = directory.Parent;
            }

            var rootTestsDirectory = directory;
            var tsFilePath = Path.Combine(rootTestsDirectory.FullName, @"Sources/AutoGeneration/Typescript", tsFileName);

            if (areEquivalent)
            {
                CsTsAssert.CheckEnum<TestEnum>(tsFilePath);
            }
            else
            {
                Assert.Throws<AssertionException>(() => CsTsAssert.CheckEnum<TestEnum>(tsFilePath));
            }
        }
    }
}
