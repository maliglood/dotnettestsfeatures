﻿export enum TestEnum {
    FirstValue = 1,
    SecondValue = 2,
    ThirdValue = 3,
    ExceededValue = 999
}