﻿/** It's comment for Test Enum */
export enum TestEnum {
    /**
     * It's multiline's comment.
     */
    FirstValue = 1,

    // This comment is like in C#.
    SecondValue = 2,

    ThirdValue = 3 // After-member-comment

    // ExceededValue_1 = 1001 // First commented member

    /*
     ExceededValue_2 = 1002,
     ExceededValue_3 = 1003
     */
}