﻿namespace DotNetTestsFeatures.Tests.Sources.Sync.Typescript
{
    internal enum TestEnum
    {
        FirstValue = 1,
        SecondValue = 2,
        ThirdValue = 3
    }
}
