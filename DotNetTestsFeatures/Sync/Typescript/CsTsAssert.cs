﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using NUnit.Framework;
using Zu.TypeScript;
using Zu.TypeScript.TsTypes;

namespace DotNetTestsFeatures.Sync.Typescript
{
    public static class CsTsAssert
    {
        /// <summary>
        /// Checks C#-enum and their TypeScript's equivalent.
        /// </summary>
        /// <typeparam name="TEnum"> Type of check C#-enum. </typeparam>
        /// <param name="tsFilePath"> Absolute path to TypeScript's file with enum. </param>
        /// <param name="tsEnumName"> Name of enum if file has more than 1 enums. </param>
        /// <returns> True if files are equivalents. </returns>
        public static void CheckEnum<TEnum>(string tsFilePath, string tsEnumName = "")
        {
            #region Get_Typescript_Enum_Members

            var source = File.ReadAllText(tsFilePath);
            var ast = new TypeScriptAST(source, tsFilePath);
            var enums = new List<EnumDeclaration>();
            EnumDeclaration @enum = null;

            foreach (var module in ast.GetDescendants().OfType<ModuleDeclaration>())
            {
                enums.AddRange(module.Body.Children.OfType<EnumDeclaration>());
            }

            enums.AddRange(ast.GetDescendants().OfType<EnumDeclaration>());

            @enum = enums.Count == 1
                ? enums.First()
                : enums.SingleOrDefault(e => e.Name.GetText() == tsEnumName);

            if (@enum == null)
                throw new Exception($"Enum {tsEnumName} wasn't found in file {tsFilePath}");

            foreach (var member in @enum.Members)
            {

            }

            var tsEnumValues = @enum.Members.Select(member => member.IdentifierStr);

            #endregion

            #region Get_C#_Enum_Members

            var csEnumType = typeof(TEnum);
            var csEnumMembers = Enum.GetValues(csEnumType)
                .Cast<TEnum>()
                .ToList();

            var csEnumValues = csEnumMembers
                .Select(csEnumMember => csEnumMember.ToString())
                .ToList();

            #endregion

            CollectionAssert.AreEquivalent(tsEnumValues, csEnumValues);
        }
    }
}
